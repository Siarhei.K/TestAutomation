import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class HomePage {

    private WebDriver driver;


    public HomePage(ChromeDriver driver) {
        this.driver = driver;
    }

    public ResultPage search(String text) {

        driver.findElement(By.id("search_from_str")).sendKeys(text);
        driver.findElement(By.xpath(".//*[@id='search_from_str']")).sendKeys(Keys.RETURN);
        return new ResultPage(driver);
    }

    public NewPage proceedToLogin() {
        driver.findElement(By.className("enter")).click();
        return new NewPage(driver);
    }
}
