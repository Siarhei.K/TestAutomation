import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by java-student1 on 23.5.17.
 */
public class NewPage {
    private WebDriver driver;

    public NewPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isNewCheckBoxChecked() {
        return driver.findElement(By.id("memory")).isSelected();
    }

    public String setLogin(String text) {
        driver.findElement(By.className("i-p")).sendKeys(text);
        //return driver.findElement(By.xpath(".//*[@id='authorize']/div/div/div/form/div[4]")).getText().length();
        return driver.findElement(By.className("i-p")).getText();
    }
}
