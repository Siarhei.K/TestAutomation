import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class ResultPage {

    private WebDriver driver;

    public ResultPage(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isCheckboxChecked() {

        return driver.findElement(By.xpath(".//*[@id='wholeSearch']")).isSelected();
    }
}
