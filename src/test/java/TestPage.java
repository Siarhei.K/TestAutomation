import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static java.lang.Boolean.TRUE;
import static org.junit.Assert.*;

public class TestPage {

    private String url = "http://tut.by";
    private ChromeDriver driver;

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(url);
    }

    @After
    public void closePage() {
        driver.quit();
    }

    @Test
    public void testTitle() {
        HomePage home = new HomePage(driver);
        assertTrue(driver.getTitle().contains("BY"));
    }

    @Test
    public void testCheckbox() {
        HomePage home = new HomePage(driver);
        ResultPage result = home.search("it");
        assertEquals(TRUE, result.isCheckboxChecked());
    }

    @Test
    public void testNewPage() {
        HomePage home = new HomePage(driver);
        NewPage newPage = home.proceedToLogin();
        assertEquals(TRUE, newPage.isNewCheckBoxChecked());
    }

    @Test
    public void testLogin() {
        HomePage home = new HomePage(driver);
        NewPage newPage = home.proceedToLogin();
        String loginText = newPage.setLogin("Sergey");
        assertEquals("Sergey", loginText);
    }

}
